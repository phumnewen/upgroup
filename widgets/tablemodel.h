//
// Created by vit on 2/14/21.
//

#ifndef UPGROUP_TABLEMODEL_H
#define UPGROUP_TABLEMODEL_H

#include "abstracttablemodel.h"


class TableModel : public AbstractTableModel
{
Q_OBJECT

public:
    TableModel(std::shared_ptr<UnitConverter> uConv, QObject *parent = nullptr);


    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;

};


#endif //UPGROUP_TABLEMODEL_H
