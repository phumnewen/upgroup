#include "abstracttablemodel.h"

AbstractTableModel::AbstractTableModel(std::shared_ptr<UnitConverter> uConv, QObject *parent):
    QAbstractItemModel (parent),
    uconv(uConv)
{
    _rowIcon.addFile(QApplication::applicationDirPath() + "/icons/open_menu.png");
    undoStack = new QUndoStack(this);
}

AbstractTableModel::~AbstractTableModel()
{
    if (undoStack)
        delete undoStack;
}

QModelIndex AbstractTableModel::parent(const QModelIndex &) const
{
    return QModelIndex();
}

QVariant AbstractTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Vertical)
    {
        if(role == Qt::DecorationRole)
            return _rowIcon;
    }
    if (orientation == Qt::Horizontal)
    {
        if (role == Qt::DisplayRole)
        {
            if (section>=0 && section<colNames.size())
            {
                QString unitView = ""; //QString::fromStdString(uconv->view(colUnits[section].toStdString()));
                QString txt = colNames[section] + "\n" + unitView;
                return txt;
            }
        }
        else if (role == Qt::FontRole)
            return QFont("DeJaVu", 8);
    }
    return QVariant();
}

int AbstractTableModel::columnCount(const QModelIndex &index) const
{
    Q_UNUSED(index)
    return colCategories.size();
}

Qt::ItemFlags AbstractTableModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

bool AbstractTableModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    bool res = true;
    for (; row<row+count; ++row)
    {
        if (!insertRow(row, parent))
            res = false;
    }
    return res;
}

bool AbstractTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    bool res = true;
    for (int i=row+count-1; i>=0; i--)
    {
        if (!removeRow(i, parent))
            res = false;
    }
    return res;
}

//void AbstractTableModel::sort(int col, Qt::SortOrder order)
//{
//    QVector <QVariant> sortKeys;
//}

std::map<std::string, std::string> AbstractTableModel::getColumnUnitList(int column) const
{
//    return column >=0 && column < colCategories.count()
//           ? uconv->unitViewsOfCategory(colCategories[column])
//           : std::map<std::string, std::string> ();
    return std::map<std::string, std::string> ();
}

void AbstractTableModel::slot_unitChanged(int column, QString unit)
{
//    if (column>=0 && column<colUnits.size())
//        colUnits[column] = unit;
//
//    const QModelIndex &beginOfColumn = this->index(0, column);
//    const QModelIndex &endOfColumn = this->index(rowCount()-1, column);
//    emit dataChanged(beginOfColumn, endOfColumn);
//    emit headerDataChanged(Qt::Horizontal, column, column);
}

void AbstractTableModel::slot_refreshData()
{
    beginResetModel();
    endResetModel();
}

void AbstractTableModel::saveAsCsv(const std::string &filePath) const
{
    TableData data (rowCount());

    TableRowData r (columnCount());
    for (int col=0; col<columnCount(); ++col)
    {
        QString name = colNames.at(col);
        name = name.replace("\n", " "), + ": " + colUnits.at(col);
        r.emplace_back(name);
    }
    data.insert(data.begin(), r);

    for (int row=0; row<rowCount(); ++row)
    {
        TableRowData r (columnCount());
        for (int col=0; col<columnCount(); ++col)
        {
            r[col] = this->data(index(row, col), Qt::DisplayRole);
        }
        data[row] = r;
    }

    QString action = tr("Сохранение таблицы в CSV");

    if (!CsvWriter::write(filePath, data, ','))
    {
        // NotificationLogManager::error(tr("Ошибка сохранения в CSV файл"), tr("Файл %1 не найден или отсутствуют права доступа").arg(filePath), action);
    }
    else
    {
        // NotificationLogManager::success(tr("Таблица сохранена"), tr("Сохранение таблицы в файл %1 CSV прошёл успешно").arg(filePath), action);
    }
}

void AbstractTableModel::loadFromCsv(const std::string &filePath)
{
    TableData data;
    int headerRowCount = 1;


    int readCode = CsvReader::read(filePath, data, headerRowCount);

//    QString action = tr("Импорт таблицы из CSV");
    switch (readCode)
    {
    case -1:
        // NotificationLogManager::error(tr("Ошибка импорта из CSV файла"), tr("Файл %1 не найден или отсутствуют права доступа").arg(filePath), action);
        break;
    case -2:
        // NotificationLogManager::error(tr("Ошибка импорта из CSV файла"), tr("Неверный формат файла %1").arg(filePath), action);
        break;
    case 0:
        // NotificationLogManager::success(tr("Таблица импортирована"), tr("Импорт таблицы из CSV файла %1 прошел успешно").arg(filePath), action);
        break;
    default:
        break;
    }

    int rc = data.size();
    int cc = 0;

    for (const auto &row: data)
        if (row.size()>cc)
            cc = row.size();

    if (cc > columnCount())
        cc = columnCount();

    if (rc>rowCount())
        while (rowCount() <rc)
            insertRow(rowCount());

    for (int row=0; row<rc; ++row)
    {
        for (int col=0; col<cc; ++col)
        {
            setData(index(row, col), data[row][col], Qt::EditRole);
        }
    }
}

void AbstractTableModel::copyToClipboard(QModelIndexList indexes) const
{
    if (indexes.isEmpty())
        return;

    std::sort(indexes.begin(), indexes.end(), [](const QModelIndex &a, const QModelIndex &b)
    {
        if (a.row() < b.row())
            return true;
        if (a.row() > b.row())
            return false;
        if (a.column() < b.column())
            return true;
        return false;
    });

    QClipboard *clipboard = QGuiApplication::clipboard();

    QString text;

    int r = indexes.first().row();
    for (const auto & index: indexes)
    {
        if (index.row()>r)
        {
            text += "\n";
            r = index.row();
        }
        else
        {
            text += "\t";
        }
        text += data(index, Qt::DisplayRole).toString();
    }

    clipboard->setText(text);
}

void AbstractTableModel::pasteFromClipboard(const QModelIndex &currentIndex)
{
    // MS Excel и я разделяем столбцы табуляцией. По ней и буду здесь парсить
    if (!currentIndex.isValid())
        return;

    int row = currentIndex.row();
    int col = currentIndex.column();

    QString text = QGuiApplication::clipboard()->text();

    QStringList lines = text.trimmed().split("\n");

    int rowsToAdd = row + lines.size() - rowCount();
    for (int i=0; i<rowsToAdd; ++i)
    {
        insertRow(rowCount());
    }

    for (int r = 0; r<lines.size(); ++r)
    {
        QStringList words = lines[r].split("\t");
        int cc = std::min(columnCount()-col, words.size());

        for (int c = 0; c < cc; ++c)
        {
            setData(index(row + r, col + c), words[c], Qt::EditRole);
        }
    }
}

void AbstractTableModel::emitDataChanged(const QModelIndex &index, int role)
{
    emit dataChanged(index, index, {role});
}

void AbstractTableModel::emitDataChanged(const int row, const int col, int role)
{
    QModelIndex idx = index(row, col);
    emit dataChanged(idx, idx, {role});
}

