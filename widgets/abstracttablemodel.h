#ifndef ABSTRACTTABLEMODEL_H
#define ABSTRACTTABLEMODEL_H

#include <QApplication>
#include <QAbstractItemModel>
#include <QObject>
#include <QIcon>
#include <QFont>
#include <QUndoStack>
#include <QDebug>
#include <QClipboard>

#include "engines/unitconverter.h"

// #include "engines/e_notificationmanager.h"

#include "engines/csvreader.h"
#include "engines/csvwriter.h"

/** @class AbstractTableModel
 * @brief Абстрактный класс модели табличных данных
 *
 * Класс для реализации паттерна Model/View в Qt
 *
 * Обязательные для реализации чистые виртуальные методы:
 *     inline QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
 *     QVariant data(const QModelIndex &index, int role) const override;
 *     int rowCount(const QModelIndex &parent = QModelIndex()) const override;
 *     bool setData(const QModelIndex &index, const QVariant &value, int role) override;
 */


class AbstractTableModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit AbstractTableModel(std::shared_ptr<UnitConverter> uConv, QObject *parent = nullptr);
    ~AbstractTableModel() override;

    QModelIndex parent(const QModelIndex &child) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;


    /// --------------------
    /// Обязательные методы:

        QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override = 0;
        QVariant data(const QModelIndex &index, int role) const override = 0;
        int rowCount(const QModelIndex &parent = QModelIndex()) const override = 0;
        bool setData(const QModelIndex &index, const QVariant &value, int role) override = 0;
    /// --------------------



    /// --------------------
    /// Необязательные методы:
    int columnCount(const QModelIndex &index = QModelIndex()) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeRows(int row, int count, const QModelIndex &parent =  QModelIndex()) override;
    /// --------------------


//    void sort (int col, Qt::SortOrder order = Qt::DescendingOrder) override;

    // Возвращает список представлений-имён единиц из категории заданной колонки
    //           ID           NAME
    std::map<std::string, std::string> getColumnUnitList (int column) const;
    QString colName (int col) const { return col>=0 && col< colNames.size() ? colNames[col] : ""; }
    inline QUndoStack * getUndoStack () const { return undoStack; }

signals:

    void saveAsCsvRequest (const QString & filePath, const QString & data) const;
//    void loadFromCsvRequest (const QString & filePath);

public slots:

    // Изменение единицы измерения колонки
    void slot_unitChanged(int column, QString unit);
    // Если данные обновлены извне и нужно вьюшку обновиить
    void slot_refreshData ();

    virtual void saveAsCsv (const std::string & filePath) const;
    virtual void loadFromCsv (const std::string & filePath);

    virtual void copyToClipboard (QModelIndexList indexes) const;
    virtual void pasteFromClipboard (const QModelIndex &currentIndex);

    virtual void sortAscending () {}
    virtual void sortDescending () {}

protected:

    virtual void emitDataChanged(const QModelIndex &index, int role = Qt::DisplayRole);
    virtual void emitDataChanged(const int row, const int col, int role = Qt::DisplayRole);

    // Конвертер единиц
    std::shared_ptr <UnitConverter> uconv;

    QStringList rowNames;
    QStringList colNames;
    QStringList colCategories;
    QStringList colUnits;
    QIcon _rowIcon;
    QUndoStack * undoStack;


};

#endif // ABSTRACTTABLEMODEL_H
