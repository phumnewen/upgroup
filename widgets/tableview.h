#ifndef TABLIEVIEW_H
#define TABLIEVIEW_H

#include <QTableView>
#include <QHeaderView>
#include <QMenu>
#include <QKeyEvent>
#include <QFontMetrics>
#include <QLineEdit>
#include <QStyledItemDelegate>
#include <QPainter>
#include <QFileDialog>

#include "abstracttablemodel.h"

class CellDelegate;

class TableView : public QTableView
{
    Q_OBJECT

public:

    TableView(QWidget *parent = nullptr);
    virtual ~TableView ();

    void setModel(QAbstractItemModel *model);
    AbstractTableModel *model() const;

signals:

    void signal_unitChanged(int column, QString unit);
    void signal_modelSet (QAbstractItemModel *model);
    void signal_CtrlEnterPressed(const QModelIndex &index);

public slots:

    void slot_insert();
    void slot_append();
    void slot_remove();

    void slot_sortAscending ();
    void slot_sortDescending ();

protected slots:

    void keyPressEvent(QKeyEvent *event);
    void resizeEvent (QResizeEvent *event);
    void showEvent (QShowEvent *event);

    virtual void saveAsCsv ();
    virtual void loadFromCsv ();

    virtual void cellMenuActions (const QModelIndex & index, QMenu * menu);

    // Отображение контекстного меню для выбора единиц измерения
    virtual void slot_showUnitsMenu (const QPoint &pos);
    // Отображение контекстного меню для действий со строкой
    virtual void slot_showRowMenu (const QPoint &pos);
    // Отображение контекстного меню для действий с ячейкой
    virtual void slot_showCellMenu (const QPoint &pos);

    // Запрос на добавление новой строки
    void slot_requestNewRowKey (const QModelIndex &index = QModelIndex());
    void slot_requestNewRowMouse (const QPoint &pos);

    /// ---------------------------
    /// Обязательно для реализации:
    // Слот обработки двойного щелчка на ячейке
    void slot_itemDoubleClicked (const QModelIndex &/*index*/) {}

    // Вставляет строку в произвольное место или добавляет в конец, если row == rowCount()
    void insertRow (int row);
    void removeRow (int row);
    /// ---------------------------

    void resizeColumns (bool reset = false);

protected:

    static constexpr int minColWidth = 50;
    bool initial = true;

    // Делегат, отвечающий за отображение ячейки в т.ч. при редактировании
    CellDelegate * _delegate = nullptr;

    AbstractTableModel * _model = nullptr;

};



class CellDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:

    explicit CellDelegate(QObject *parent = 0) : QStyledItemDelegate(parent) {}

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const override;

};

#endif // TABLIEVIEW_H
