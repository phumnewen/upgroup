#include "tableview.h"

TableView::TableView(QWidget *parent) : QTableView (parent)
{

    _delegate = new CellDelegate ();
    setItemDelegate(_delegate);
    //    setStyleSheet("QTableView { selection-background-color: transparent; }");

    // Если начинаешь вводить, включается редактирование
    setEditTriggers(QAbstractItemView::AnyKeyPressed | QAbstractItemView::DoubleClicked);

    setSortingEnabled(false);
    setWordWrap(false);
    setContextMenuPolicy(Qt::CustomContextMenu);

    horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
    horizontalHeader()->setStretchLastSection(false);
    horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(horizontalHeader(), &QHeaderView::customContextMenuRequested, this, &TableView::slot_showUnitsMenu);
    connect(verticalHeader(), &QHeaderView::customContextMenuRequested, this, &TableView::slot_showRowMenu);
    connect(this, &QTableView::customContextMenuRequested, this, &TableView::slot_showCellMenu);

    connect(this, &TableView::doubleClicked, this, &TableView::slot_itemDoubleClicked);
    connect(this, &QTableView::customContextMenuRequested, this, &TableView::slot_requestNewRowMouse);
    connect(this, &TableView::signal_CtrlEnterPressed, this, &TableView::slot_requestNewRowKey);
}

TableView::~TableView()
{
    //    setItemDelegate(nullptr);
    _delegate->deleteLater();
}

void TableView::setModel(QAbstractItemModel *model)
{
    if (!dynamic_cast<AbstractTableModel*>(model))
        return;
    _model = dynamic_cast<AbstractTableModel*>(model);

    QTableView::setModel(model);
    connect(this, &TableView::signal_unitChanged, dynamic_cast<AbstractTableModel*>(model), &AbstractTableModel::slot_unitChanged);
    resizeColumns(true);
    emit signal_modelSet(model);
}

AbstractTableModel *TableView::model() const
{
    return _model;
}

void TableView::keyPressEvent(QKeyEvent *event)
{
    if (!model())
        return;

    int row = currentIndex().row();
    int col = currentIndex().column();

    int key = event->key();
    Qt::KeyboardModifiers mod = event->modifiers();

    // Ctrl+Enter используется для добавления строки
    if ((key == Qt::Key_Enter || key == Qt::Key_Return) && mod == Qt::ControlModifier)
    {
        emit signal_CtrlEnterPressed(currentIndex());
    }
    else if (mod == Qt::NoModifier)
    {
        //  Enter для перехода на новую строку
        switch (key)
        {
        case Qt::Key_Enter:
            if (row<model()->rowCount()-1)
                setCurrentIndex(model()->index(row+1, col));
            break;
        case Qt::Key_Return:
            if (row<model()->rowCount()-1)
                setCurrentIndex(model()->index(row+1, col));
            break;
        }
    }

    // Ctrl+C копирование
    else if (key == Qt::Key_C && mod == Qt::ControlModifier)
    {
        dynamic_cast<AbstractTableModel*>(model())->copyToClipboard(selectedIndexes());
    }

    // Ctrl+V вставка
    else if (key == Qt::Key_V && mod == Qt::ControlModifier)
    {
        dynamic_cast<AbstractTableModel*>(model())->pasteFromClipboard(currentIndex());
    }

    // Здесь стандартная навигация клавишами
    QTableView::keyPressEvent(event);
}

void TableView::resizeEvent(QResizeEvent *event)
{
    resizeColumns(false);
    QTableView::resizeEvent(event);
}

void TableView::showEvent(QShowEvent *event)
{
    resizeColumns(initial);
    initial = false;

    QTableView::showEvent(event);
}

void TableView::saveAsCsv()
{
    std::string filePath = QFileDialog::getSaveFileName(this, tr("Сохранение таблицы в CSV"), QDir::currentPath()).toStdString();
//    QDir::setCurrent(filePath);

    if (!filePath.empty())
    {
        if (model())
            dynamic_cast<AbstractTableModel*>(model())->saveAsCsv(filePath);
    }
}

void TableView::loadFromCsv()
{
    std::string filePath = QFileDialog::getOpenFileName(this, tr("Заполнение таблицы из CSV"), QDir::currentPath()).toStdString();
//    QDir::setCurrent(filePath);

    if (!filePath.empty())
    {
        if (model())
            dynamic_cast<AbstractTableModel*>(model())->loadFromCsv(filePath);
    }
}

void TableView::cellMenuActions(const QModelIndex &index, QMenu *menu)
{
    if (index.isValid())
    {
        // Создаём два действия: "Вставить" и "Удалить"
        QAction *copyAct = new QAction(tr("Копировать"), menu);
        QAction *pasteAct = new QAction(tr("Вставить"), menu);
        QAction *insAct = new QAction(QIcon("icons/insert_hc.png"), tr("Вставить строку"), menu);
        QAction *addAct = new QAction(QIcon("icons/add_hc.png"), tr("Добавить строку"), menu);
        QAction *rmAct = new QAction(QIcon("icons/remove_hc.png"), tr("Удалить строку"), menu);

        // Добавляем пояснения к действиям
        copyAct->setToolTip("Копировать данные в буфер обмена");
        pasteAct->setToolTip("Вставить текст из буфера обмена");
        insAct->setToolTip("Вставить строку перед текущей");
        addAct->setToolTip("Добавить строку в конец");
        rmAct->setToolTip("Удалить текующую строку");

        // Добавляем действия в меню
        menu->addAction(copyAct);
        menu->addAction(pasteAct);
        menu->addAction(insAct);
        menu->addAction(addAct);
        menu->addAction(rmAct);


        if (model()) {
            connect(copyAct, &QAction::triggered, this,
                    [&]() { dynamic_cast<AbstractTableModel *>(model())->copyToClipboard(selectedIndexes()); });
            connect(pasteAct, &QAction::triggered, this,
                    [&]() { dynamic_cast<AbstractTableModel *>(model())->pasteFromClipboard(currentIndex()); });
            connect(insAct, &QAction::triggered, this, [&]() { insertRow(index.row()); });
            connect(addAct, &QAction::triggered, this, [&]() { insertRow(model()->rowCount()); });
            connect(rmAct, &QAction::triggered, this, [&]() { removeRow(index.row()); });
        }
    }

    QAction   *saveAsCsvAct = new QAction(QIcon("icons/saveCsv.png"), tr("Сохранить как CSV"), menu);
    QAction *loadFromCsvAct = new QAction(QIcon("icons/loadCsv.png"), tr("Заполнить из CSV"),  menu);

    saveAsCsvAct->setToolTip("Сохранить в текстовую таблицу с разделителем \",\"");
    loadFromCsvAct->setToolTip("Заполнить из текстовой таблицы с разделителем \",\"");

    connect (saveAsCsvAct, &QAction::triggered, this, &TableView::saveAsCsv);
    connect (loadFromCsvAct, &QAction::triggered, this, &TableView::loadFromCsv);

    menu->addAction(saveAsCsvAct);
    menu->addAction(loadFromCsvAct);
}

void TableView::slot_showUnitsMenu(const QPoint &pos)
{
    if (!model())
        return;

    QModelIndex index = indexAt(pos);

    if(index.isValid())
    {
        int col = index.column();
        // Собственно контекстное меню
        QMenu menu;
        // Список действий меню
        QList<QAction*> actions;
        // Указатель на действие
        QAction *action = nullptr;

        std::map<std::string, std::string> units = dynamic_cast<AbstractTableModel*>(model())->getColumnUnitList(col);
        for (auto un: units)
        {
            // Представление единицы измерения
            action = new QAction(QString::fromStdString(un.second), &menu);
            // Идентификатор единицы измерения
            action->setData(QString::fromStdString(un.first));
            actions.push_back(action);
        }
        // Добавляем все дейтвия в меню
        menu.addActions(actions);

        // Меню выполняется, если хоть одно действие задано
        if(actions.size() > 0)
        {
            QAction *selected = menu.exec(cursor().pos());
            // Если выбрано одно из двух действий
            if (selected)
                emit signal_unitChanged (col, selected->data().toString());
        }
    }
}

void TableView::slot_showRowMenu(const QPoint &pos)
{
    QModelIndex index = indexAt(pos);

    QMenu mn(this);

    if (index.isValid())
    {
        // Создаём два действия: "Вставить" и "Удалить"
        QAction *insAct = new QAction(QIcon("icons/insert_hc.png"), tr("Вставить строку"), this);
        QAction *addAct = new QAction(QIcon("icons/add_hc.png"), tr("Добавить строку"), this);
        QAction *rmAct = new QAction(QIcon("icons/remove_hc.png"), tr("Удалить строку"), this);

        // Добавляем пояснения к действиям
        insAct->setToolTip("Вставить строку перед текущей");
        addAct->setToolTip("Добавить строку в конец");
        rmAct->setToolTip("Удалить текующую строку");

        // Добавляем действия в меню
        mn.addAction(insAct);
        mn.addAction(addAct);
        mn.addAction(rmAct);

        connect(insAct, &QAction::triggered, this, [&](){ insertRow(index.row()); } );
        connect(rmAct,  &QAction::triggered, this, [&](){ removeRow(index.row()); } );
        if (model())
            connect(addAct, &QAction::triggered, this, [&](){ insertRow(model()->rowCount()); } );
    }

    mn.exec(cursor().pos());
}

void TableView::slot_showCellMenu(const QPoint &pos)
{
    QModelIndex index = indexAt(pos);

    QMenu mn(this);

    cellMenuActions(index, &mn);
    mn.exec(cursor().pos());
}

void TableView::slot_requestNewRowKey(const QModelIndex &/*index*/)
{
    if (model())
        insertRow(model()->rowCount());
}

void TableView::slot_requestNewRowMouse(const QPoint &pos)
{
    if (indexAt(pos).isValid())
        return;

    if (model())
        insertRow(model()->rowCount());
}

void TableView::insertRow(int row)
{
    if (model())
        model()->insertRow(row);
}

void TableView::removeRow(int row)
{
    if (model())
        model()->removeRow(row);
}

void TableView::resizeColumns(bool reset)
{
    if (!model())
        return;

    int c = model()->columnCount();
    if (c <= 0)
        return;
    std::vector<int> colWidths (c);

    const AbstractTableModel *mdl = dynamic_cast<AbstractTableModel*>(model());

    int tableWidth = this->width() - verticalHeader()->width();
    int colWTtl = 0;

    QFontMetrics mtr (horizontalHeader()->font());

    for (int i=0; i<c; ++i)
    {
        int w;
        if (reset)
        {
            int maxW = 0;
            QStringList rows = mdl->colName(i).split("\n");
            for (const auto &r: rows)
            {
                int cw = mtr.width(r);
                if (cw>maxW)
                    maxW = cw;
            }
            // Ширина колонки будет пропорциональна размеру надписи в её заголовке
            w = maxW;
        }
        else
        {
            // Ширина колонки будет пропорциональна текущей ширине (при resize)
            w = columnWidth(i);
        }
        colWTtl += w;
        colWidths [i] = w;
    }

    if (colWTtl<=0)
        colWTtl = tableWidth / c;

    double rat = static_cast<double>(tableWidth) / static_cast<double>(colWTtl);

    for (int i=0; i<c; ++i)
    {
        int cw = static_cast<int>(static_cast<double>(colWidths[i]) * rat);
        int colW = cw < minColWidth ? minColWidth : cw;
        setColumnWidth(i, colW);
    }
}

void TableView::slot_insert()
{
    insertRow(currentIndex().row());
}

void TableView::slot_append()
{
    if (model())
        insertRow(model()->rowCount());
}

void TableView::slot_remove()
{
    QVector<int> selectedRows;

    for (const auto &i: selectedIndexes())
        if (!selectedRows.contains(i.row()))
            selectedRows.push_back(i.row());

    for (int i=0; i<selectedRows.size(); i++)
        for (int j=i; j<selectedRows.size(); j++)
            if (selectedRows[j] < selectedRows[i])
            {
                int t = selectedRows[i];
                selectedRows[i] = selectedRows[j];
                selectedRows[j] = t;
            }

    if (selectedRows.size()>1)
    {
//        if (selectedRows.contains(model()->rowCount()-1))
//        {
//            QVector<int> tmp(selectedRows.size());
//            for (int i=selectedRows.size()-1; i>=0; i--)
//                tmp[selectedRows.size()-1-i] = selectedRows[i];
//            selectedRows = tmp;
//        }
        for (int i=selectedRows.size()-1; i>=0; i--)
        {
            removeRow(selectedRows[i]);
        }
    }

    else
    {
        removeRow(currentIndex().row());
    }
}

void TableView::slot_sortAscending()
{
    if (model())
        dynamic_cast<AbstractTableModel*>(model())->sortAscending();
}

void TableView::slot_sortDescending()
{
    if (model())
        dynamic_cast<AbstractTableModel*>(model())->sortDescending();
}


//-------------------------------------
//-- DELEGATE -------------------------
//-------------------------------------


QWidget *CellDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const
{
    QLineEdit *editor = new QLineEdit (parent);
    editor->setFrame(false);
    return editor;
}

void CellDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QString data = index.model()->data(index, Qt::DisplayRole).toString();
    QLineEdit *le = dynamic_cast<QLineEdit*>(editor);
    le->setText(data);
}

void CellDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QLineEdit *le = static_cast<QLineEdit*>(editor);
    QString text = le->text();
    model->setData(index, text, Qt::EditRole);
}

void CellDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
    editor->setGeometry(option.rect);
}

void CellDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyledItemDelegate::paint(painter, option, index);

    QVariant background = index.data(Qt::BackgroundRole);
    if (background.canConvert<QBrush>())
        painter->fillRect(option.rect, background.value<QBrush>());
    // the comment below makes selection transparent
    QStyledItemDelegate::paint(painter, option, index);
    // To draw a border on selected cells
    if(option.state & QStyle::State_Selected)
    {
        painter->save();
        QPen pen(Qt::darkGray, 1, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin);
        int w = pen.width()/2;
        painter->setPen(pen);
        painter->drawRect(option.rect.adjusted(w,w,-w,-w));
        painter->restore();
    }
}



