#ifndef UPPARAMSDIALOG_H
#define UPPARAMSDIALOG_H

#include <QDialog>

namespace Ui {
class UpParamsDialog;
}

class UpParamsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UpParamsDialog(QWidget *parent = nullptr);
    ~UpParamsDialog();

private:
    Ui::UpParamsDialog *ui;
};

#endif // UPPARAMSDIALOG_H
