#ifndef I_IMPORTCSVPYROLYSISDATADIALOG_H
#define I_IMPORTCSVPYROLYSISDATADIALOG_H

#include <QDialog>
#include <QFile>
#include <QFileDialog>

namespace Ui {
class LoadCSVDialog;
}

class LoadCSVDialog : public QDialog
{
    Q_OBJECT

public:

    explicit LoadCSVDialog(QWidget *parent = nullptr);
    ~LoadCSVDialog();

    int headerLineNumber () const;
    QString delimiter() const;
    QString fileName() const;

private slots:

    void on_toolButton_openFile_released();

    void on_toolButton_released();

private:

    Ui::LoadCSVDialog *ui;

    QString path;

    void reread ();

    double _heatingRate = 20;
};

#endif // I_IMPORTCSVPYROLYSISDATADIALOG_H
