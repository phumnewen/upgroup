#include "upparamsdialog.h"
#include "ui_upparamsdialog.h"

UpParamsDialog::UpParamsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpParamsDialog)
{
    ui->setupUi(this);
}

UpParamsDialog::~UpParamsDialog()
{
    delete ui;
}
