//
// Created by vit on 2/14/21.
//

#include "tablemodel.h"

TableModel::TableModel(std::shared_ptr<UnitConverter> uConv, QObject *parent)
: AbstractTableModel(uConv, parent)
{

}

QModelIndex TableModel::index(int row, int column, const QModelIndex &parent) const {
    return QModelIndex();
}

QVariant TableModel::data(const QModelIndex &index, int role) const {
    return QVariant();
}

int TableModel::rowCount(const QModelIndex &parent) const {
    return 0;
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    return false;
}
