#include "loadcsvdialog.h"
#include "ui_loadcsvdialog.h"

LoadCSVDialog::LoadCSVDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoadCSVDialog)
{
    ui->setupUi(this);

    ui->toolButton_updatePreview->setIcon (QIcon ("icons/redo.png"));
    ui->toolButton_openFile->setIcon (QIcon ("icons/open.png"));
}

LoadCSVDialog::~LoadCSVDialog()
{
    delete ui;
}

int LoadCSVDialog::headerLineNumber() const
{
    return ui->spinBox_headerLinesNumber->value();
}

QString LoadCSVDialog::delimiter() const
{
    return ui->lineEdit_delimiter->text();
}

QString LoadCSVDialog::fileName() const
{
    return ui->lineEdit_filePath->text();
}

void LoadCSVDialog::on_toolButton_openFile_released()
{
    path = QFileDialog::getOpenFileName(this, tr("Загрузка данных пиролиза из CSV"), QDir::currentPath(), "Текстовые таблицы (*.csv *.tsv *.dsv *.txt)");

    ui->lineEdit_filePath->setText(path);
    QFileInfo fi(path);
    QDir::setCurrent(fi.absolutePath());

    reread();
}

void LoadCSVDialog::on_toolButton_released()
{
    reread();
}

void LoadCSVDialog::reread()
{
    QFile fl (path);
    if (!fl.open(QIODevice::ReadOnly))
    {
        fl.close();
        return;
    }
    QString txt;
    for (int i=0; i<ui->spinBox_previewRowCount->value(); i++)
        txt += QString::fromLatin1(fl.readLine());
    ui->textEdit_preview->setText(txt);
}
