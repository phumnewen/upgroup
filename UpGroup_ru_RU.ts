<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="UpGroup_ru_RU">
<context>
    <name>AbstractTableModel</name>
    <message>
        <location filename="widgets/abstracttablemodel.cpp" line="134"/>
        <source>Сохранение таблицы в CSV</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadCSVDialog</name>
    <message>
        <location filename="widgets/loadcsvdialog.ui" line="14"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_loadcsvdialog.h" line="156"/>
        <source>Импорт таблицы CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/loadcsvdialog.ui" line="20"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_loadcsvdialog.h" line="157"/>
        <source>Открыть файл</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/loadcsvdialog.ui" line="32"/>
        <location filename="widgets/loadcsvdialog.ui" line="115"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_loadcsvdialog.h" line="158"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_loadcsvdialog.h" line="167"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/loadcsvdialog.ui" line="45"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_loadcsvdialog.h" line="160"/>
        <source>Строки заголовка будут проигнорированы при импорте</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/loadcsvdialog.ui" line="48"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_loadcsvdialog.h" line="162"/>
        <source>Количество строк заголовка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/loadcsvdialog.ui" line="62"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_loadcsvdialog.h" line="163"/>
        <source>Разделитель</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/loadcsvdialog.ui" line="69"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_loadcsvdialog.h" line="164"/>
        <source>;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/loadcsvdialog.ui" line="76"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_loadcsvdialog.h" line="165"/>
        <source>Предпросмотр файла</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/loadcsvdialog.ui" line="93"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_loadcsvdialog.h" line="166"/>
        <source>Количество строк предпросмотра</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/loadcsvdialog.cpp" line="36"/>
        <source>Загрузка данных пиролиза из CSV</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="build/UpGroup_autogen/include/ui_mainwindow.h" line="50"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_mainwindow.h" line="79"/>
        <source>Межгрупповая статистика</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_mainwindow.h" line="89"/>
        <source>Файл</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_mainwindow.h" line="90"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="52"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_mainwindow.h" line="80"/>
        <source>Up2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="57"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_mainwindow.h" line="81"/>
        <source>Импорт CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="60"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_mainwindow.h" line="83"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="65"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_mainwindow.h" line="85"/>
        <source>Выход</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <location filename="cmake-build-debug/UpGroup_autogen/include/ui_mainwindow.h" line="87"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TableView</name>
    <message>
        <location filename="widgets/tableview.cpp" line="120"/>
        <source>Сохранение таблицы в CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/tableview.cpp" line="132"/>
        <source>Заполнение таблицы из CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/tableview.cpp" line="147"/>
        <source>Копировать</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/tableview.cpp" line="148"/>
        <source>Вставить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/tableview.cpp" line="149"/>
        <location filename="widgets/tableview.cpp" line="241"/>
        <source>Вставить строку</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/tableview.cpp" line="150"/>
        <location filename="widgets/tableview.cpp" line="242"/>
        <source>Добавить строку</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/tableview.cpp" line="151"/>
        <location filename="widgets/tableview.cpp" line="243"/>
        <source>Удалить строку</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/tableview.cpp" line="179"/>
        <source>Сохранить как CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/tableview.cpp" line="180"/>
        <source>Заполнить из CSV</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpParamsDialog</name>
    <message>
        <location filename="widgets/upparamsdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/upparamsdialog.ui" line="20"/>
        <source>Колонка имён</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/upparamsdialog.ui" line="30"/>
        <source>Колонка для анализа</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgets/upparamsdialog.ui" line="50"/>
        <source>Доверительный интервал</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
