#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
        : QMainWindow(parent)
        , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    table = new TableView (this);
    setCentralWidget(table);

    connect(ui->action_quit, &QAction::triggered, this, &MainWindow::close);
    connect(ui->action_importCSV, &QAction::triggered, this, &MainWindow::showCsvLoadingDialog);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showCsvLoadingDialog() {
    LoadCSVDialog dlg (this);

    if (QDialog::Accepted == dlg.exec()) {
        emit loadCsvRequest(dlg.fileName(), dlg.delimiter(), dlg.headerLineNumber());
    }
}



