#include "csvreader.h"

int CsvReader::read(const std::string &fileName, TableData &data, const char & delimiter, const int headerRowCount)
{

    std::ifstream file(fileName.c_str(), std::ios_base::in );

    if(!file)
    {
        //  Файл недоступен
        return -1;
    }

    try
    {
        std::string line;

        // Пропускаем заголовок
        for (int i=0; i<headerRowCount; i++)
        {
            std::getline(file, line);
        }

        data.clear();
        while(std::getline(file, line))
        {
            QString line_q = QString::fromStdString(line);
            TableRowData lineData = split(line_q, delimiter);
            data.push_back(lineData);
        }
    }
    catch (...)
    {
        file.close();
        // Ошибочный формат файла
        return -2;
    }

    file.close();
    return 0;
}

TableRowData CsvReader::split(const QString &s, const char &delimiter)
{
    TableRowData data;
    auto lst = s.split(delimiter, Qt::KeepEmptyParts);

    for (const auto &w: lst){
        data.emplace_back(w);
    }

    return data;
}
