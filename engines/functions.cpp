#include "functions.h"

double Functions::uPiFreq(const std::vector<double> &a1, const std::vector<double> &a2)
{

    size_t n1 = a1.size();
    size_t n2 = a2.size();

    int sum1 = std::accumulate(a1.begin(), a1.end(), 0);
    int sum2 = std::accumulate(a2.begin(), a2.end(), 0);
    if (n1 == 0 || n2 == 0 || (sum1 == 0 && sum2 == 0))
    {
        return 0;
    }

    std::vector<double> a = a1;
    a.insert(a.end(), a2.begin(), a2.end());

    size_t n = a.size();

    std::vector<double> a_sq (n);

    for (size_t i=0; i<n; ++i)
    {
        a_sq[i] = pow (a[i], 2);
    }

    int sum_sq = std::accumulate(a_sq.begin(), a_sq.end(), 0);
    int sum = std::accumulate(a.begin(), a.end(), 0);

    double mul1 = (n1 + n2 - 1) / ( (n1 + n2) * n1 * n2 );
    double mul2num = static_cast<double>(pow(n2 * static_cast<double>(sum1)
                                             - n1 * static_cast<double>(sum2), 2)); // * (n2 * sum1 - n1 * sum2)
    double mul2den = sum_sq - pow(sum, 2) * 1/n;

    //            if mul2den == 0:
    //            print ("A: ", A)
    //            print ("sum A2: ", pow(sum(A), 2))
    //            print ("AA: ", AA)
    //            print ("sum AA: ", sum(AA))
    //            print ("1/N: ", 1/n)
    //            return 0

    return mul1 * mul2num / mul2den;
}

void Functions::dataDivider(const std::vector<double> &a, size_t i, std::vector<double> &a1,  std::vector<double> &a2)
{
    if (i>0 and i<a.size())
    {
        a1 = std::vector<double> (a.begin(), a.begin()+i);
        a2 = std::vector<double> (a.begin()+i, a.end());
    }
}

std::vector<double> Functions::divideIterator(const std::vector<double> &a, std::vector<double> &clusters)
{
    size_t n = a.size();
    std::vector<double> hiSq (n);

    int maxHiI = -1;
    double maxHi = 3.84;

    for (size_t i=0; i<n; ++i)
    {
        std::vector<double> a1;
        std::vector<double> a2;

        dataDivider(a, i, a1, a2);
        double hi = uPiFreq(a1, a2);
        hiSq[i] = hi;
        if (hi > maxHi)
        {
            maxHi = hi;
            maxHiI = i;
        }
    }

    if (maxHiI >= 0)
    {

        std::vector<double> a1;
        std::vector<double> a2;
        dataDivider(a, maxHiI, a1, a2);
        divideIterator (a1, clusters);
        divideIterator (a2, clusters);
    }
    else
    {
        clusters.insert(clusters.end(), a.begin(), a.end());
    }

    return hiSq;
}




