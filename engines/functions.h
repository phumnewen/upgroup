#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <vector>
#include <numeric>
#include <cmath>

class Functions
{
public:

    static double uPiFreq (const std::vector<double> &a1, const std::vector<double> &a2);

    static void dataDivider (const std::vector<double> &a, size_t i, std::vector<double> &a1, std::vector<double> &a2);

    static std::vector<double> divideIterator(const std::vector<double> &a, std::vector<double> &clusters);

};

#endif // FUNCTIONS_H
