//
// Created by vit on 2/15/21.
//

#include "top_manager.h"

TopManager::TopManager(QObject *parent) : QObject(parent)
{
    w = new MainWindow();
    w->setAttribute(Qt::WA_DeleteOnClose);

    t = new TableModel (nullptr, this);

    connect (w, &MainWindow::loadCsvRequest, this, &TopManager::runCsvLoader);
}

void TopManager::showMainWindow()
{
    w->show();
}

void TopManager::runCsvLoader(const QString &fileName, const QString &delimiter, const int linesToSkip) {
    TableData data;
    char del = delimiter.toStdString().front();
    CsvReader::read (fileName.toStdString(), data, del, linesToSkip);
}
