#include "unitsmap.h"

UnitsMap::UnitsMap(QString fileName)
{
    valid = true;
    if (!readUnitsFile(fileName))
    {
        valid = false;
    }
}

UnitsMap::~UnitsMap()
{
    for (auto val: unitsMap)
    {
        delete val.second;
    }
}

bool UnitsMap::readUnitsFile(const QString &fileName)
{

    QFile readFile(fileName);
    // Пытаемся открыть файл на чтение
    if (!readFile.open(QIODevice::ReadOnly))
    {
        // Если не получилось, закрываем его
        readFile.close();
        // И возвращаем false
        return false;
    }

    // Открываем поток от файла
    QTextStream fileStream(&readFile);
    // Задаём кодировку
    fileStream.setCodec("UTF-8");

    QStringList cat;
    // Метка незаполненной базовой единицы
    QString  emptyBase = "ept";
    bool inCat = false;

    // Пока файл не кончился
    while(!fileStream.atEnd())
    {
        // Читаем файл построчно
        QString line = fileStream.readLine();

        // Символ комментария или пустая строка пропускаются
        if (line.isEmpty() || line.trimmed().isEmpty() || line.at(0) == '#')
        {
            continue;
        }

        if (inCat)
        {
            if (line.at(0) == '[')
            {
                inCat = false;
            }

            else
            {
                // Парсим строку единицы по пробелам, удаляя лишние
                QStringList lst = line.trimmed().split(" ", QString::SkipEmptyParts);
                // Булка для проверки корректности преобразования строки в число
                bool okTan;
                // Угловой коеффициент
                double tan = lst.last().toDouble(&okTan);
                if (okTan)
                {
                    // Заполняем значения единицы
                    Unit *unt = new Unit();
                    unt->coeff = tan;
                    unt->category = cat.first().trimmed();
                    unitsMap[lst.first().trimmed()] = unt;
                    if (lst.size() == 2)
                        unitViews[lst.first().trimmed().toStdString()] = lst.first().trimmed().toStdString();
                    else if (lst.size() == 3) // Новый формат, где второй колонкой представление
                        unitViews[lst.first().trimmed().toStdString()] = lst[1].trimmed().toStdString();

                    // Если базовая единица не была заполнена при определении категории,
                    // то ею становится имя, значение которого равно 1
                        if (categories.at(cat.first().trimmed()) == emptyBase
                            && std::numeric_limits<double>::epsilon() > fabs(tan - 1))
                    {
                        categories[cat.first().trimmed()] = lst.first().trimmed();
                    }
                }
            }
        }

        if (!inCat)
        {
            // Удаляем скобочки
            line.remove('[');
            line.remove(']');
            cat = line.trimmed().split(',', QString::SkipEmptyParts);

            // Определяется категория величин (длина/время/давление и т.д.)
            // Если в файле при категории не задана базовая единица
            if (cat.size() == 1)
            {
                // Помечаем базовую единицу пустой
                categories[cat.first().trimmed()] = emptyBase;
            }

            // Если задана базовая единица
            else
            {
                // Заполняем бузовую единицу
                categories[cat.first().trimmed()] = cat.last().trimmed();
            }
            inCat = true;
        }
    }

    return true;
}
