#include "csvwriter.h"

bool CsvWriter::write(const std::string &fileName, const TableData &data, const char & delimiter)
{
    std::ofstream file(fileName.c_str(), std::ios_base::out | std::ios_base::trunc);

    if(!file)
    {
        return false;
    }

    for (const auto &line: data)
    {
        for (const auto &word: line)
        {
            file << word.toString().toStdString() << delimiter;
//            std::visit([&](const auto &elem) { file << elem << delimiter; }, word);
        }
        file << '\n';
    }

    file.close();
    return true;
}
