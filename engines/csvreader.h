#ifndef CSVTOTABLREADER_H
#define CSVTOTABLREADER_H

#include <fstream>
#include <sstream>
//
//#include <QString>
//#include <QFile>
//#include <QTextStream>

#include "data/table_data.h"

class CsvReader
{
public:
    static int read(const std::string &fileName, TableData &data, const char & delimiter, const int headerRowCount = 0);
    static TableRowData split (const QString &s, const char & delimiter);
};

#endif // CSVTOTABLREADER_H
