//
// Created by vit on 2/15/21.
//

#ifndef UPGROUP_TOP_MANAGER_H
#define UPGROUP_TOP_MANAGER_H

#include "widgets/tablemodel.h"
#include "mainwindow.h"


class TopManager : public QObject
{
    Q_OBJECT

public:

    TopManager (QObject * parent = nullptr);

public slots:

    void showMainWindow ();

private slots:

    void runCsvLoader (const QString &fileName, const QString &delimiter, const int linesToSkip);

private:

    MainWindow *w = nullptr;
    TableModel *t = nullptr;

};


#endif //UPGROUP_TOP_MANAGER_H
