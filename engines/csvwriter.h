#ifndef CSVWRITER_H
#define CSVWRITER_H

#include <fstream>

#include "data/table_data.h"

class CsvWriter
{
public:
    static bool write(const std::string &fileName, const TableData &data, const char & delimiter);
};

#endif // CSVWRITER_H
