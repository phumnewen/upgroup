#ifndef UNITSMAP_H
#define UNITSMAP_H

#include <limits>
#include <cmath>
#include <map>
#include <memory>

#include <QString>
#include <QVector>
#include <QFile>
#include <QTextStream>

/** @class UnitsMap
 * @brief Словарь единиц измерения.
 * Читает файлик с единицами и формирует словари идентификаторов, имен-представлений единиц и категорий единиц
 * Формат файла:
 *    # комментарий
 *    [CATEGORY_NAME, base_unit_id]  # Имя категории едениц измерения, строка-идентификатор базовой единицы, которая назначается по умолчанию
 *    unit_id <coeff>                # Строка-идентификатор (уникальная) единицы, коффициент единицы (для базовой обычно = 1)
 *    unit_id unit_view <coeff>      # Если три записи в строке, то вторая будет считана в словарь представлений (если две, то представление равно id)
 */

class UnitsMap
{

public:

    UnitsMap(QString fileName);
    ~UnitsMap();


    //! @brief Отдельная единица измерения
    struct Unit
    {
        double coeff;   //!< Коэффициент в пропорции
        QString category;   //!< Категория единицы измерения
    };


    //! @brief Списки единиц измерения их переводных коэффициентов
    std::map < QString, Unit* > unitsMap;
    //! @brief Списки единиц измерения их представлений-имён
    std::map < std::string, std::string > unitViews;
    //! @brief Список категорий с базовыми единицами для них
    std::map <QString, QString> categories;
//    //! @brief Базовые единицы в каждой категории (на будующее)
//    QMap <QString, QString> baseUnits;


    //! @brief Чтение единиц из файла
    bool readUnitsFile(const QString &fileName);

    //! @brief Количество единиц
    inline int unitsCount() const { return unitsMap.size(); }

    //! @brief Получение значения по названию единицы измерения
    inline const Unit    &unit (const QString &unitId) const { return *unitsMap.at(unitId); }

    //! @brief Возвращает состояние ваилдности
    //!
    //! false, если файл единиц не был корректно прочитан
    inline bool isValid() const { return valid; }


//    //! @brief Задать базовые единицы (на будующее)
//    void setBaseUnits();


private:

    bool valid;

};

#endif // UNITSMAP_H
