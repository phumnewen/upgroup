#ifndef UNITCONVERTER_H
#define UNITCONVERTER_H

#include <QString>
#include <QDebug>

#include "unitsmap.h"


// Конвертер единиц измерения

class UnitConverter
{

public:

    UnitConverter(const QString &fileName = ":/converter_files/units_en.txt");
    // Копирующий конструктор
    UnitConverter(const UnitConverter &converter);
    ~UnitConverter();


    /// Общие методы

    // Возвращает состояние валидности
    inline bool isValid() { return valid; }

    // Возвращает список названий единиц, относящихся к заданной категории
    QStringList unitsOfCategory(const QString &category);

    // Возвращает список представлений-имён единиц, относящихся к заданной категории
    std::map<std::string, std::string> unitViewsOfCategory(const QString &category);

    // Имя-представление единицы измерения

    ///GETs:

    // Возвращает словарь единиц измерения
//    eUnitsMap *unitsMap() const { return um; }
    // Возвращает базовую единицу для категории
    const QString &baseUnit(const QString &category) const { return um->categories.at(category); }

    // Имя-представление единицы с unitId
    inline const std::string &view (const std::string &unitId) const { return um->unitViews.count(unitId)>0 ? um->unitViews.at(unitId) : empty; }



    /// Методы конвертации

    // Простая конвертация
    double convert(const double &val,const QString &from, const QString &to) const;
    QVector <double> convert(const QVector <double> vals, const QString from, const QString to) const;

    // Отдельно -- для температуры
    double convertTemperature(const double val,const QString from, const QString to) const;
    QVector <double> convertTemperature(const QVector <double> vals, const QString from, const QString to) const;

    // Конвертация в базовые единицы измереия
    //    double convertFrom(const double val,const QString from, const QString to) const;
    //    QVector <double> convertFrom(const QVector <double> vals, const QString from, const QString to) const;

    // Конвертация из базовых единицы измереия
    //    double convertTo(const double val,const QString from, const QString to) const;
    //    QVector <double> convertTo(const QVector <double> vals, const QString from, const QString to) const;

    /// Задание базовых единиц измерения (на будующее)


protected:

    // Константы для названий единиц температуры
    const QString celsius  = "C";
    const QString faringate = "F";
    const QString kelvin = "K";

    // Коэффициент для конвертации
    inline double coeff(QString from, QString to) const {return um->unit(from).coeff / um->unit(to).coeff;}

    // Проверка, что катеории конвертируемых величин совпадают
    bool sameCategory(QString from, QString to) const;

    /// Базовые единицы измерения (на будующее)


private:

    // Сотстояние валидности конвертера (в основном зависит от того, найден ли указанный файл единиц измерения)
    bool valid;

    // Словарь единиц измерения
    UnitsMap *um;

    static const std::string empty;


};


#endif // UNITCONVERTER_H
