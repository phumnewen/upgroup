#include "unitconverter.h"

const std::string UnitConverter::empty = "";

UnitConverter::UnitConverter(const QString &fileName)
{
    valid = true;
    um = new UnitsMap(fileName);
    // Проверка на успешную инициализацию словаря единиц
    if (!um->isValid())
    {
        valid = false;
    }
}

UnitConverter::UnitConverter(const UnitConverter &converter)
{
    this->um = converter.um;
}

UnitConverter::~UnitConverter()
{
    delete um;
}


///-------------------------------------------------------------------------------------------------------------


QStringList UnitConverter::unitsOfCategory(const QString &category)
{
    QStringList lsunits;
    // Перебор всех единиц, чтобы найти принадлежащие к заданной категории
    for (auto p: um->unitsMap)
    {
        auto item = p.second;
        if (item->category == category)
        {
            // И запись их в список
            lsunits.push_back(p.first);
        }
    }
    return lsunits;
}

std::map < std::string, std::string > UnitConverter::unitViewsOfCategory(const QString &category)
{
    std::map < std::string, std::string > catViews;
    // Перебор всех единиц, чтобы найти принадлежащие к заданной категории
    for (auto p: um->unitViews)
    {
        std::string id = p.first;
        std::string view = p.second;

        // И запись их в список
        if (um->unitsMap[QString::fromStdString(id)]->category == category)
            catViews[id] = view;
    }
    return catViews;
}


///-------------------------------------------------------------------------------------------------------------


bool UnitConverter::sameCategory(QString from, QString to) const
{
    // проверка на совпадение категорий единиц
    if (um->unit(from).category != um->unit(to).category)
        return false;

    return true;
}


///-------------------------------------------------------------------------------------------------------------


double UnitConverter::convert(const double &val, const QString &from, const QString &to) const
{
    // Проверка, что единицы из одной категории
    if (from == "" || to == "" || !sameCategory(from, to))
    {
        return val;
    }

    // Получаем коэффициент и домножаем на него конвертируемое значение
    return  val * coeff(from, to);
}


QVector <double> UnitConverter::convert(const QVector <double> vals, const QString from, const QString to) const
{
    // Выходной вектор
    QVector <double> result;

    // Проверка, что итемы из одной категории
    if (!sameCategory(from, to))
    {
        return result;
    }

    // Коэффииент вычисляется перед циклом, чтобы не делать это в каждой итерации
    double coef = coeff(from, to);

    for (auto val: vals)
    {
        // Вычисляем значения и записываем их в выходной вектор
        result.push_back(coef * val);
    }
    return result;
}


///-------------------------------------------------------------------------------------------------------------


double UnitConverter::convertTemperature(const double val, const QString from, const QString to) const
{
    // Проверка, что единицы из одной категории
    if (!sameCategory(from, to))
    {
        return 0;
    }

    double coef = coeff(from, to);

    // Далее все случаи перевода

    // Из Цельсия
    if (from == celsius && to == celsius) // В Цельсий
    {
        return val;
    }
    else if (from == celsius && to == faringate) // В Фарингейт
    {
        return val * coef + 32;
    }
    else if (from == celsius && to == kelvin)    // В Кельвин
    {
        return val + 273;
    }

    // Из Фарингейта
    if (from == faringate && to == faringate) // В Фарингейт
    {
        return val;
    }
    else if (from == faringate && to == celsius) // В Цельсий
    {
        return val * coef - 32;
    }
    else if (from == faringate && to == kelvin)    // В Кельвин
    {
        return val * coef - 32 + 273;
    }

    // Из Кельвина
    if (from == kelvin && to == kelvin)    // В Кельвин
    {
        return val;
    }
    else if (from == kelvin && to == celsius)    // В Цельсий
    {
        return val - 273;
    }
    else if (from == kelvin && to == faringate) // В Фарингейт
    {
        return (val - 273) * coef - 32;
    }

    return 0;
}


QVector<double> UnitConverter::convertTemperature(const QVector<double> vals, const QString from, const QString to) const
{
    // Выходной вектор
    QVector <double> v;
    // Проверка, что единицы из одной категории
    if (!sameCategory(from, to))
    {
        // Если единицы из разных категорий, возвращается пустой вектор
        return v;
    }

    // Коэффииент вычисляется перед циклом, чтобы не делать это в каждой итерации
    double coef = 9;

    // Далее все случаи перевода

    // Из Цельсия
    if (from == celsius && to == celsius) // В Цельсий
    {
        for (auto val: vals)
        {
            v.push_back(val);
        }
    }
    else if (from == celsius && to == faringate) // В Фарингейт
    {
        for (auto val: vals)
        {
            v.push_back(val * coef + 32);
        }
    }
    else if (from == celsius && to == kelvin)    // В Кельвин
    {
        for (auto val: vals)
        {
            v.push_back(val + 273);
        }
    }

    // Из Фарингейта
    if (from == faringate && to == faringate) // В Фарингейт
        {
            for (auto val: vals)
            {
                v.push_back(val);
            }
        }
    else if (from == faringate && to == celsius) // В Цельсий
    {
        for (auto val: vals)
        {
            v.push_back(val * coef - 32);
        }
    }
    else if (from == faringate && to == kelvin)  // В Кельвин
    {
        for (auto val: vals)
        {
            v.push_back(val * coef - 32 + 273);
        }
    }

    // Из Кельвина
    if (from == kelvin && to == kelvin)    // В Кельвин
        {
            for (auto val: vals)
            {
                v.push_back(val);
            }
        }
    else if (from == kelvin && to == celsius)    // В Цельсий
    {
        for (auto val: vals)
        {
            v.push_back(val - 273);
        }
    }
    else if (from == kelvin && to == faringate)  // В Фарингейт
    {
        for (auto val: vals)
        {
            v.push_back((val - 273) * coef - 32);
        }
    }
    return v;

}


