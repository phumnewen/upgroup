#include "engines/top_manager.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TopManager m;
    m.showMainWindow ();
    return a.exec();
}
