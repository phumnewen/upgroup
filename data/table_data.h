//
// Created by vit on 2/14/21.
//

#ifndef UPGROUP_TABLE_DATA_H
#define UPGROUP_TABLE_DATA_H

#include <vector>
#include <QVariant>

typedef std::vector<QVariant> TableRowData;
typedef std::vector< TableRowData > TableData;

#endif //UPGROUP_TABLE_DATA_H
